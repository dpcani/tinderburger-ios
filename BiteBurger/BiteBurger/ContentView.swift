//
//  ContentView.swift
//  BiteBurger
//
//  Created by Usuario on 10/3/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Bite Burger")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
