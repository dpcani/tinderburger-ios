//
//  BiteBurgerApp.swift
//  BiteBurger
//
//  Created by Usuario on 10/3/22.
//

import SwiftUI

@main
struct BiteBurgerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
